import { enableProdMode } from '@angular/core';

import { environment } from './environments/environment';
import * as domino from 'domino';
import * as fs from 'fs';
import * as path from 'path';
declare var global: any;
const template = fs.readFileSync(path.join(__dirname, '../browser', 'index.html')).toString();
const win = domino.createWindow(template);
global.window = win;
global.document = win.document;

if (environment.production) {
  enableProdMode();
}

export { AppServerModule } from './app/app.server.module';
export { renderModule, renderModuleFactory } from '@angular/platform-server';
